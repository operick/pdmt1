package iteso.com.sesion5;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import iteso.com.sesion5.utils.commons;

public class ActivityMain extends AppCompatActivity {

    EditText name;
    EditText phone;

    Spinner grade;
    RadioGroup gender;
    AutoCompleteTextView book;
    CheckBox practiceSports;

    private Student student;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initializeViews();

        // inflate objects
        name =  findViewById(R.id.activity_main_input_name);
        phone =  findViewById(R.id.activity_main_input_phone);
        grade = findViewById(R.id.activity_main_grade_spinner);
        book = findViewById(R.id.activity_main_auto_complete_book);
        practiceSports = findViewById(R.id.activity_main_practice_sport);

        // initialize student
        student = new Student("", "", "", "", "", false);

    }

    @Override
    protected void onStart() {
        super.onStart();
        grade.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int pos, long id) {
                String grade = (String) parent.getItemAtPosition(pos);
                student.setSchoolLevel(grade);
            }
            @Override
            public void onNothingSelected(AdapterView<?> parentView) {
                // your code here
            }

        });
    }

    private void initializeViews(){
        // Inflate AutoCompleteTextView object
        AutoCompleteTextView booksAutoComplete = (AutoCompleteTextView)findViewById(R.id.activity_main_auto_complete_book);

        // get book suggestions for auto complete text box
        String[] bookSuggestions = getResources().getStringArray(R.array.activity_main_book_auto_complete);

        // create adapter for array of book suggestions
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this,
                android.R.layout.simple_dropdown_item_1line, bookSuggestions);

        // display text Country
        booksAutoComplete.setAdapter(adapter);
    }
    public void onPracticeSports(View view) {
        student.setPracticeSport(practiceSports.isChecked()?true:false);
    }
    public void onRadioButtonClicked(View view) {
        // Is the button now checked?
        boolean checked = ((RadioButton) view).isChecked();

        // Check which radio button was clicked
        switch(view.getId()) {
            case R.id.activity_main_female:
                if (checked)
                    student.setGender("female");
                    break;
            case R.id.activity_main_male:
                if (checked)
                    student.setGender("male");
                    break;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.activity_main_save:
                saveInformation();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void saveInformation(){
        student.setName("" + name.getText());
        student.setPhone("" + phone.getText());
        student.setFavoriteBook("" + book.getText());

        Toast.makeText(ActivityMain.this,student.toString(),Toast.LENGTH_SHORT).show();

    }
    public void clearInformation(View v){
        AlertDialog alertDialog = new AlertDialog.Builder(ActivityMain.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage(getResources().getString(R.string.activity_main_clean_dialog));
        alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, getResources().getString(R.string.activity_main_clean_cancel),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, getResources().getString(R.string.activity_main_clean_accept),
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        name.setText("");
                        phone.setText("");
                        grade.setSelection(0);
                        RadioButton femaleBtn = (RadioButton) findViewById(R.id.activity_main_female);
                        femaleBtn.setChecked(true);
                        book.setText("");
                        practiceSports.setChecked(false);
                        dialog.dismiss();
                    }
                });
        alertDialog.show();
    }
}
