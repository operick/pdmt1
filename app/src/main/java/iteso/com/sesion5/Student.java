package iteso.com.sesion5;

public class Student {
    private String name;
    private String phone;
    private String schoolLevel;
    private String gender;
    private String favoriteBook;
    private Boolean practiceSport;

    public Student(String name, String phone, String schoolLevel, String gender, String favoriteBook, Boolean practiceSport) {
        this.name = name;
        this.phone = phone;
        this.schoolLevel = schoolLevel;
        this.gender = gender;
        this.favoriteBook = favoriteBook;
        this.practiceSport = practiceSport;
    }

    @Override
    public String toString() {
        return "Nombre:" + name + '\n' +
                "Telefono:" + phone + '\n' +
                "Escolaridad:" + schoolLevel + '\n' +
                "Genero:" + gender + '\n' +
                "Libro Favorito:" + favoriteBook + '\n' +
                "Practica Deporte:" + (practiceSport ? "Si": "No" );
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getSchoolLevel() {
        return schoolLevel;
    }

    public void setSchoolLevel(String schoolLevel) {
        this.schoolLevel = schoolLevel;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getFavoriteBook() {
        return favoriteBook;
    }

    public void setFavoriteBook(String favoriteBook) {
        this.favoriteBook = favoriteBook;
    }

    public Boolean getPracticeSport() {
        return practiceSport;
    }

    public void setPracticeSport(Boolean practiceSport) {
        this.practiceSport = practiceSport;
    }
}
